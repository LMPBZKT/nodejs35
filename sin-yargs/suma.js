const fs = require('fs')


let salida = '';

console.log('=====================');
console.log('      Sumar          ');
console.log('=====================');


const suma = 2;
for (let i = 1; i <= 100; i++) {
    salida += `${suma} + ${i} = ${suma + i}\n`;
}

console.log(salida);

fs.writeFile(`../salida-operaciones/Suma-${suma}.txt`, salida, (err) => {
    if (err) throw err;
    console.log(`table-${suma} creado`);
});
