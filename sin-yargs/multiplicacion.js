const fs = require('fs')

const multiplicar = 10;
let salida = '';

console.log('=====================');
console.log('      Multiplicar    ');
console.log('=====================');



for (let i = 1; i <= 100; i++) {
    salida += `${multiplicar} x ${i} = ${multiplicar * i}\n`;
}

console.log(salida);

fs.writeFile(`../salida-operaciones/Multiplicacion-${multiplicar}.txt`, salida, (err) => {
    if (err) throw err;
    console.log(`table-${multiplicar} creado`);
});

