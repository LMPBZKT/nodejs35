const fs = require('fs')

const resta = 10;
let salida = '';

console.log('=====================');
console.log('      Restar         ');
console.log('=====================');


for (let i = 1; i <= 100; i++) {
    salida += `${resta} - ${i} = ${resta - i}\n`;
}

console.log(salida);

fs.writeFile(`../salida-operaciones/Resta-${resta}.txt`, salida, (err) => {
    if (err) throw err;
    console.log(`table-${resta} creado`);
});