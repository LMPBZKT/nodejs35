const fs = require('fs')
const crearArchivo = (dividir = 5) => {

try {

let salida = '';

console.log('=====================');
console.log('      Dividir         ');
console.log('=====================');
for (let i = 1; i <= 100; i++) {
    salida += `${dividir} / ${i} = ${dividir / i}\n`;
}

console.log(salida);

fs.writeFile(`../salida-operaciones/divicion-${dividir}.txt`, salida, (err) => {
    if (err) throw err;
    console.log(`table-${dividir} creado`);
});
}
 catch (error) {
  throw error;
 }



module.exports = {
     generarArchivo: crearArchivo
    };
    
}
